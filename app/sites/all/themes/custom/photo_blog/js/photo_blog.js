(function ($) {
    Drupal.behaviors.photoBlog = {
      attach: function (context, settings) {
        $(window).load(function() {
          $('.flexslider').flexslider();
        });
        // Code to be run on page load, and
        // on ajax load added here
      }
    };
  }(jQuery));