<?php
  
function photo_blog_preprocess_page(&$variables){
  $page = $variables["page"];
  $body_class = "full-width";
  $column_class = "";

  // if the left column and the right column are empty
  // else if the left column or right column are not empty
  if(empty($page["left_column"]) && empty($page["right_column"])){
    $column_class = "";
    $body_class = "full-width";
  }
  elseif (!empty($page["left_column"]) || !empty($page["right_column"])) {
    $column_class = "one-fourth";
    $body_class = "half";
  }




  $variables["photo_blog"]["body_class"] = $body_class;
  $variables["photo_blog"]["column_class"] = $column_class;
  
}
